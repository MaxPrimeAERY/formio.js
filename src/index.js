// DO NOT SAY "export *"!!! This messes up the umd for dist builds.
import app from 'chance';

export { Builders, Components, Displays, Providers, Rules, Widgets, Templates, Utils, Form, Conjunctions, Operators, QuickRules, Transformers, ValueSources, Formio } from './formio.form';
export FormBuilder from './FormBuilder';

app.set('view engine', 'ejs');
